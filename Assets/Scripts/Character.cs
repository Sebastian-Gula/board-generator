﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System.Diagnostics;

public abstract partial class Character : MonoBehaviour
{
    private MatrixNode AStar(char[][] board, Vector2 from, Vector2 to)
    {
        Dictionary<string, MatrixNode> explored = new Dictionary<string, MatrixNode>();
        Dictionary<string, MatrixNode> open = new Dictionary<string, MatrixNode>();
        List<Vector2> neighbors = new List<Vector2>();

        neighbors.Add(new Vector2(-1, 0));
        neighbors.Add(new Vector2(0, 1));
        neighbors.Add(new Vector2(1, 0));
        neighbors.Add(new Vector2(0, -1));

        MatrixNode startNode = new MatrixNode(from, to);
        string key = from.x.ToString() + from.y.ToString();
        open.Add(key, startNode);

        while (open.Any()) 
        {
            KeyValuePair<string, MatrixNode> smallest = SmallestNode(open);

            foreach (var neighbor in neighbors)
            {
                Vector2 position = smallest.Value.Position + neighbor;

                int x = (int)position.x;
                int y = (int)position.y;

                if (x > 0 && x < board.Length && y > 0 && y < board.Length && board[x][y] != 'X')
                {
                    MatrixNode current = new MatrixNode(position, to, smallest.Value);

                    if (position == to)
                        return current;

                    key = current.Position.x.ToString() + current.Position.y.ToString();
                    if (!open.ContainsKey(key) && !explored.ContainsKey(key))
                        open.Add(key, current);
                }
            }

            open.Remove(smallest.Key);
            explored.Add(smallest.Key, smallest.Value);
        }

        return null;
    }

    protected List<Vector2> shortestPath(char[][] matrix, int fromX, int fromY, int toX, int toY)
    {
        Vector2 from = new Vector2(fromX, fromY);
        Vector2 to = new Vector2(toX, toY);

        if (from == to || matrix[toX][toY] == 'X')
            return new List<Vector2>();

        MatrixNode end = AStar(matrix, from, to);

        if (end != null)
        {
            List<Vector2> next = new List<Vector2>();
            Stack<MatrixNode> path = new Stack<MatrixNode>();

            while (end.Parent != null)
            {
                path.Push(end);
                end = end.Parent;
            }

            path.Push(end);
            path.Pop();

            int tmpX = (int)transform.position.x;
            int tmpY = (int)transform.position.y;

            while (path.Count > 0)
            {
                MatrixNode node = path.Pop();

                int x = (int)node.Position.x - tmpX;
                int y = (int)node.Position.y - tmpY;

                tmpX = (int)node.Position.x;
                tmpY = (int)node.Position.y;

                next.Add(new Vector2(x, y));
            }

            return next;
        }

        return new List<Vector2>();
    }

    private KeyValuePair<string, MatrixNode> SmallestNode(Dictionary<string, MatrixNode> open)
    {
        KeyValuePair<string, MatrixNode> smallest = open.ElementAt(0);

        foreach (KeyValuePair<string, MatrixNode> item in open)
        {
            if (item.Value.FunctionF < smallest.Value.FunctionF)
                smallest = item;
        }

        return smallest;
    }
}